export const environment = {
  production: false,
  version: '0.0.2',  
  locale: 'es',
  PLACE: 'CDMX', //Oficinas de NUUP
  LAT: 19.436821,
  LON: -99.1661614,
  // DOCS: https://home.openweathermap.org/api_keys
  API_KEY: '47bc25041f9433717134563fc6c744b0',
  API_URL: 'http://localhost:8000/api',
};
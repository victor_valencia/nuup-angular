import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {  
  
  // DOCS: https://openweathermap.org/api/one-call-api
  private URI_ONECALL = `https://api.openweathermap.org/data/2.5/onecall?units=metric&lang=es&lat=${environment.LAT}&lon=${environment.LON}&appid=${environment.API_KEY}`;

  // ICONS: https://openweathermap.org/weather-conditions

  // ICON URL: http://openweathermap.org/img/wn/{ICON_CODE}@2x.png
  
  constructor(private http: HttpClient) { }

  // Obtiene los datos de la API DE OpenWeather
  get() {

    return this.http.get(this.URI_ONECALL);

  }

  // Guardar los datos de la API Local
  post(data: any): any {    

    return this.http.post(`${environment.API_URL}/weather`, data);

  }

  // Obtiene los datos de la API Local (Historial)
  getAll(): any {

    return this.http.get(`${environment.API_URL}/weather`);

  }

}

import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { WeatherService } from './services/weather.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    
  public title = 'Pronóstico del Clima';

  public version = environment.version;
  
  public place = environment.PLACE;

  public weather: any;  

  public current: any;

  public currentTemp: number = 0;

  public currentIndex: number = -1;

  public dailyData: any;

  public historyData: any;

  constructor(private service: WeatherService){

  }  

  ngOnInit(): void {
  
    this.service.get().subscribe(
      (res: any) => { 

        console.log(res) 
        this.weather = res;
        this.current = this.weather.current;
        this.currentTemp = this.weather.current.temp;
        this.dailyData = this.weather.daily.slice(1,6);

        this.saveData(res);

      }, 
      (err: any) => console.log(err)
    );

  }

  saveData(data: any) {

    const now = new Date();
    const now_str = `${now.toISOString().substring(0,10)} ${now.toLocaleTimeString('es-ES')}`;

    console.log(now, now_str);
    this.service.post({date: now_str, temp: data.current.temp, data: JSON.stringify(data)}).subscribe(
      (res: any) => { 

        console.log("SE GUARDARON LOS DATOS CON EXITO", res);
        
      }, 
      (err: any) => console.log(err)
    );

  }

  changeCurrent(index: number) {

    this.current = this.dailyData[index];
    this.currentTemp = this.current.temp.day;
    this.currentIndex = index;

  }

  showHistory() {

    this.service.getAll().subscribe(
      (res: any) => { 

        console.log(res) 
        this.historyData = res;

      }, 
      (err: any) => console.log(err)
    );

  }

}

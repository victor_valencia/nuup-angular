module.exports = {

  base: '/api',
  pages: {
    docs: true,
    monitor: true
  },
  routes: {
    tb: [
      {
        //  Weather ['GET', 'POST', 'PUT', 'DELETE', 'SEARCH']
        table: 'weather',
        event: 'Weather',   
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'SEARCH'],   
        columns: [
          {name: 'id', primary: true},
          {name: 'date'},
          {name: 'temp'},
          {name: 'data'}         
        ]
      }
    ]
 }

}
const fs = require('fs');
const api = require('./api.js');
const pkg = require('./package.json');

const rest_doc = require('rest-docs');
var rest = new rest_doc();

rest.startServer()
const knex = rest.startDBServer()
rest.buildRoutes(api)

// Extend Server
const app = rest.app;

const express = require('express');
const path = require('path');

app.use(express.static(__dirname + '/dist/nuup-angular'));

app.get('/*', function(req, res){  
  res.sendFile(path.join(__dirname + '/dist/nuup-angular/index.html'));
});
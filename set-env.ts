var fs = require('fs');
var pkg = require('./package.json');
var env = require('dotenv').config();
const colors = require('colors');

const getFile = function(value: boolean): string {
  // `environment.ts` file structure
  return `export const environment = {
  production: ${value},
  version: '${pkg.version}',  
  locale: 'es',
  PLACE: 'CDMX', //Oficinas de NUUP
  LAT: 19.436821,
  LON: -99.1661614,
  // DOCS: https://home.openweathermap.org/api_keys
  API_KEY: '47bc25041f9433717134563fc6c744b0',
  API_URL: '${process.env['API_URL']}',
};`;
}

const writeFile = function(name: string, file: string) {

  const targetPath = `./src/environments/${name}`;
  console.log(colors.magenta(`The file '${name}' will be written with the following content: \n`));
  console.log(colors.grey(file));
  fs.writeFile(targetPath, file, function (err: any) {
    if (err) {
        throw console.error(err);
    } else {
        console.log(colors.magenta(`Angular '${name}' file generated correctly at ${targetPath} \n`));
    }
  });  

}

// Configure Angular `environment.ts` file path
writeFile('environment.ts', getFile(false));

// Configure Angular `environment.prod.ts` file path
writeFile('environment.prod.ts', getFile(true));



# Nuup - Angular

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

## REQUISITOS

- `Node.js` > `v12.x`
- `NPM` > `v6.x`
- `Git` > `v2.17.`
- `Angular CLI` > `v13.x`

## CLONAR REPOSITORIO

```bash
git clone https://victor_valencia@bitbucket.org/victor_valencia/nuup-angular.git
```

## DESPLIEGUE EN AWS

```bash
ssh -i C:\Users\Victor\Downloads\victorKeys.pem ubuntu@ec2-3-142-144-218.us-east-2.compute.amazonaws.com
```

#### 1.- Acceder al servidor

```bash
ssh -i C:\Users\Victor\Downloads\victorKeys.pem ubuntu@ec2-3-142-144-218.us-east-2.compute.amazonaws.com
```

#### 2.- Cambiarse al repositorio.

```bash
cd nuup-angular
```

#### 3.- Actualizar repositorio.
```bash
git pull
```

#### 4.- Construir la aplicación (Producción)

```bash
ng build
```

#### 5.- Copiar `dist` al directorio `/var/www/html`

```bash
sudo cp -r ~/nuup-angular/dist/nuup-angular/* /var/www/html
```

## COMANDOS DE NGINX

#### Configuración

Visualizar la configuración del servidor:

```bash
sudo vi /etc/nginx/sites-available/default
```

Configuración actual del servidor:

```bash
server {
    listen 80 default_server;
    listen [::]:80 default_server;
 
    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;
 
    server_name _;
 
    location / {
        try_files $uri $uri/ /index.html =404;
    }
}
```
#### Comandos:

Reiniciar el servidor:

```bash
sudo systemctl restart nginx 
```

Revisar el estatus del servidor:

```bash
sudo systemctl status nginx
```
